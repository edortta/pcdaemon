<?php

function mil2minutes($mil) {
  $a = substr($mil,0,2);
  $b = substr($mil,2,2);
  $ret = $a*60+$b;
  return $ret;
}

function __limits($method, $entrypoint, $entityBody) {
  $configFileName = ".config/limits.json";
  $configFile     = (file_exists($configFileName) ? json_decode(file_get_contents($configFileName), true) : []);

  date_default_timezone_set(_getValue($configFile, 'TZ', 'UTC'));

  $ret        = createEmptyRet(200, 'Limits');
  $verb       = array_shift($entrypoint);
  $saveConfig = false;

  $_checkUsageEnabled = function (&$remainingTime, $limits, $now) {
    $canUse        = false;
    $n             = 0;
    while ($n < count($limits)) {
      $start = _getValue($limits[$n], 'start', '0000');
      $end   = _getValue($limits[$n], 'end', '0000');
      if ($now >= $start && $now <= $end) {
        $canUse = true;
        $aux    = mil2minutes($end) - mil2minutes($now);
        if ($aux > $remainingTime) {
          $remainingTime = $aux;
        }

      }
      $n++;
    }
    return $canUse;
  };

  switch ($verb) {
  case 'get':
    $ret['return'] = [
      'daily'     => _getValue($configFile, 'daily', []),
      'holidays'  => _getValue($configFile, 'holidays', []),
      'today'     => _getValue($configFile, 'today', []),
      'server_ts' => date("Y-m-d-H-i-s")
    ];
    $now    = date("Hi");
    $remainingTime = 0;
    $canUse1 = $_checkUsageEnabled($remainingTime, $ret['return']['daily'], $now);
    $canUse2 = $_checkUsageEnabled($remainingTime, $ret['return']['holidays'], $now);
    $canUse3 = $_checkUsageEnabled($remainingTime, $ret['return']['today'], $now);

    $canUse = $canUse1 || $canUse2 || $canUse2;

    $ret['return']['can_use'] = ($canUse ? 1 : 0);
    $ret['return']['remaining_time'] = $remainingTime;

    break;

  case 'set':
    $scope = array_shift($entrypoint);
    $n     = 0;
    while (count($entrypoint) > 0) {
      $start = str_pad(preg_replace('/\D/', '', array_shift($entrypoint)), 4, '0', STR_PAD_LEFT);
      $end   = str_pad(preg_replace('/\D/', '', array_shift($entrypoint)), 4, '0', STR_PAD_LEFT);
      if ($start > $end) {
        $aux   = $end;
        $end   = $start;
        $start = $aux;
      }

      $configFile[$scope][$n++] = ['start' => $start, 'end' => $end];
    }
    $saveConfig = true;
    break;

  case 'unset':
    $scope = array_shift($entrypoint);
    if (isset($configFile[$scope])) {
      unset($configFile[$scope]);
    }

    $saveConfig = true;

  case 'configTZ':
    $TZ1 = array_shift($entrypoint);
    $TZ2 = array_shift($entrypoint);
    if ($TZ2 > '') {
      $configFile['TZ'] = "$TZ1/$TZ2";
    } else {
      $configFile['TZ'] = "$TZ1";
    }

    $saveConfig = true;
  }

  if ($saveConfig) {
    $configSaved = false;
    if (!is_dir(".config")) {
      @mkdir(".config");
    }

    if (is_writable(".config")) {
      $configSaved = file_put_contents("$configFileName", json_encode($configFile, JSON_PRETTY_PRINT));
    }

    if (!$configSaved) {
      $ret['http_code'] = 500;
      $ret['message']   = "Folder .config cannot be written";
    }
  }

  return $ret;
}
